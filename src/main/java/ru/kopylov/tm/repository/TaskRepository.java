package ru.kopylov.tm.repository;

import ru.kopylov.tm.entity.Task;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class TaskRepository {

    public static Map<String, Task> taskMap = new LinkedHashMap<>();

    public void merge(Task task) {
        taskMap.put(task.getId(), task);
    }

    public Task persist(Task task) {
        return taskMap.putIfAbsent(task.getId(), task);
    }

    public List<Task> findAll() {
        return new ArrayList<>(taskMap.values());
    }

    public List<Task> findAll(String currentUserId) {
        List<Task> tasks = new ArrayList<>();
        for (Map.Entry<String, Task> entry : taskMap.entrySet()) {
            if (currentUserId.equals(entry.getValue().getUserId()))
                tasks.add(entry.getValue());
        }
        return tasks;
    }

    public boolean remove(String taskName) {
        return taskMap.entrySet()
                .removeIf(entry -> entry.getValue().getName().equals(taskName));
    }

    public boolean remove(String taskName, String currentUserId) {
        return taskMap.entrySet()
                .removeIf(entry -> (entry.getValue().getName().equals(taskName)
                        & entry.getValue().getUserId().equals(currentUserId)));
    }

    public void removeById(String taskId) {
        taskMap.remove(taskId);
    }

    public void removeAll() {
        taskMap.clear();
    }

    public void removeAll(String currentUserId) {
        taskMap.entrySet()
                .removeIf(entry -> entry.getValue().getUserId().equals(currentUserId));
    }

    public Task findOne(String taskName) {
        for (Map.Entry<String, Task> entry : taskMap.entrySet()) {
            if (taskName.equals(entry.getValue().getName())) {
                return entry.getValue();
            }
        }
        return null;
    }

    public Task findOne(String taskName, String currentUserId) {
        for (Map.Entry<String, Task> entry : taskMap.entrySet()) {
            if (taskName.equals(entry.getValue().getName()) & currentUserId.equals(entry.getValue().getUserId())) {
                return entry.getValue();
            }
        }
        return null;
    }

    public List<Task> findAllById(List<String> taskIdList) {
        List<Task> taskNameList = new ArrayList<>();
        for (String taskId : taskIdList) {
            taskNameList.add(TaskRepository.taskMap.get(taskId));
        }
        return taskNameList;
    }

}
