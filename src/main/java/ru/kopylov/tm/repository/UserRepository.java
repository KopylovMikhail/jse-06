package ru.kopylov.tm.repository;

import ru.kopylov.tm.entity.User;
import ru.kopylov.tm.enumerated.TypeRole;

import java.util.LinkedHashMap;
import java.util.Map;

public class UserRepository {

    public static Map<String, User> userMap = new LinkedHashMap<>();

    public void merge(User user) {
        userMap.put(user.getId(), user);
    }

    public User persist(String login, String password) {
        for (Map.Entry<String, User> entry : userMap.entrySet()) {
            if (login.equals(entry.getValue().getLogin())) return null;
        }
        User user = new User();
        user.setLogin(login);
        user.setPassword(password);
        user.setRole(TypeRole.USER);
        userMap.put(user.getId(), user);
        return user;
    }

    public User findOne(String login, String password) {
        for (Map.Entry<String, User> entry : userMap.entrySet()) {
            if (login.equals(entry.getValue().getLogin())) {
                if (password.equals(entry.getValue().getPassword()))
                    return entry.getValue();
            }
        }
        return null;
    }

    public User findById(String id) {
        return userMap.get(id);
    }

}
