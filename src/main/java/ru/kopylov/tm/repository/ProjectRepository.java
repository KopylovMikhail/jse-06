package ru.kopylov.tm.repository;

import ru.kopylov.tm.entity.Project;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ProjectRepository {

    public static Map<String, Project> projectMap = new LinkedHashMap<>();

    public void merge(Project project) {
        projectMap.put(project.getId(), project);
    }

    public Project persist(Project project) {
        return projectMap.putIfAbsent(project.getId(), project);
    }

    public List<Project> findAll() {
        return new ArrayList<>(projectMap.values());
    }

    public List<Project> findAll(String currentUserId) {
        List<Project> projects = new ArrayList<>();
        for (Map.Entry<String, Project> entry : projectMap.entrySet()) {
            if (currentUserId.equals(entry.getValue().getUserId()))
                projects.add(entry.getValue());
        }
        return projects;
    }

    public boolean remove(String projectName) {
        return projectMap.entrySet()
                .removeIf(entry -> entry.getValue().getName().equals(projectName));
    }

    public boolean remove(String projectName, String currentUserId) {
        return projectMap.entrySet()
                .removeIf(entry -> (entry.getValue().getName().equals(projectName)
                        & entry.getValue().getUserId().equals(currentUserId)));
    }

    public void removeAll() {
        projectMap.clear();
    }

    public void removeAll(String currentUserId) {
        projectMap.entrySet()
                .removeIf(entry -> entry.getValue().getUserId().equals(currentUserId));
    }

    public Project findOne(String projectName) {
        for (Map.Entry<String, Project> entry : projectMap.entrySet()) {
            if (projectName.equals(entry.getValue().getName())) {
                return entry.getValue();
            }
        }
        return null;
    }

    public Project findOne(String projectName, String currentUserId) {
        for (Map.Entry<String, Project> entry : projectMap.entrySet()) {
            if (projectName.equals(entry.getValue().getName()) & currentUserId.equals(entry.getValue().getUserId())) {
                return entry.getValue();
            }
        }
        return null;
    }

}
