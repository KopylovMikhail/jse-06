package ru.kopylov.tm.enumerated;

public enum TypeRole {

    ADMIN("administrator"),
    USER("user");

    private final String displayName;

    TypeRole(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}
