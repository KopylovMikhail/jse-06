package ru.kopylov.tm.service;

import ru.kopylov.tm.entity.Task;
import ru.kopylov.tm.repository.TaskRepository;

import java.util.List;

public class TaskService {

    private TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public boolean create(String taskName, String userId) {
        if (taskName == null || taskName.isEmpty()) return false;
        Task task = new Task();
        task.setName(taskName);
        task.setUserId(userId);
        return !task.equals(taskRepository.persist(task));
    }

    public List<Task> list() {
        return taskRepository.findAll();
    }

    public List<Task> list(String currentUserId) {
        if (currentUserId == null || currentUserId.isEmpty()) return null;
        return taskRepository.findAll(currentUserId);
    }

    public boolean update(String nameOld, String nameNew) {
        if (nameOld == null || nameOld.isEmpty()) return false;
        if (nameNew == null || nameNew.isEmpty()) return false;
        if (taskRepository.findOne(nameOld) == null) return false;
        Task task = taskRepository.findOne(nameOld);
        task.setName(nameNew);
        taskRepository.merge(task);
        return true;
    }

    public boolean update(String nameOld, String nameNew, String currentUserId) {
        if (nameOld == null || nameOld.isEmpty()) return false;
        if (nameNew == null || nameNew.isEmpty()) return false;
        if (currentUserId == null || currentUserId.isEmpty()) return false;
        if (taskRepository.findOne(nameOld, currentUserId) == null) return false;
        Task task = taskRepository.findOne(nameOld, currentUserId);
        task.setName(nameNew);
        taskRepository.merge(task);
        return true;
    }

    public boolean remove(String taskName) {
        if (taskName == null || taskName.isEmpty()) return false;
        return taskRepository.remove(taskName);
    }

    public boolean remove(String taskName, String currentUserId) {
        if (taskName == null || taskName.isEmpty()) return false;
        if (currentUserId == null || currentUserId.isEmpty()) return false;
        return taskRepository.remove(taskName, currentUserId);
    }

    public void clear() {
        taskRepository.removeAll();
    }

    public void clear(String currentUserId) {
        if (currentUserId == null || currentUserId.isEmpty()) return;
        taskRepository.removeAll(currentUserId);
    }

}
