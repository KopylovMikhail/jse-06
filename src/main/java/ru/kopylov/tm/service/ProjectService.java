package ru.kopylov.tm.service;

import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.entity.Task;
import ru.kopylov.tm.repository.ProjectRepository;
import ru.kopylov.tm.repository.TaskOwnerRepository;
import ru.kopylov.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;

public class ProjectService {

    private ProjectRepository projectRepository;

    private TaskRepository taskRepository;

    private TaskOwnerRepository taskOwnerRepository;

    public ProjectService(ProjectRepository projectRepository,
                          TaskRepository taskRepository,
                          TaskOwnerRepository taskOwnerRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
        this.taskOwnerRepository = taskOwnerRepository;
    }

    public boolean create(String projectName, String userId) {
        if (projectName == null || projectName.isEmpty()) return false;
        Project project = new Project();
        project.setName(projectName);
        project.setUserId(userId);
        return !project.equals(projectRepository.persist(project));
    }

    public List<Project> list() {
        return projectRepository.findAll();
    }

    public List<Project> list(String currentUserId) {
        if (currentUserId == null || currentUserId.isEmpty()) return null;
        return projectRepository.findAll(currentUserId);
    }

    public boolean update(String nameOld, String nameNew) {
        if (nameOld == null || nameOld.isEmpty()) return false;
        if (nameNew == null || nameNew.isEmpty()) return false;
        if (projectRepository.findOne(nameOld) == null) return false;
        Project project = projectRepository.findOne(nameOld);
        project.setName(nameNew);
        projectRepository.merge(project);
        return true;
    }

    public boolean update(String nameOld, String nameNew, String currentUserId) {
        if (nameOld == null || nameOld.isEmpty()) return false;
        if (nameNew == null || nameNew.isEmpty()) return false;
        if (currentUserId == null || currentUserId.isEmpty()) return false;
        if (projectRepository.findOne(nameOld, currentUserId) == null) return false;
        Project project = projectRepository.findOne(nameOld, currentUserId);
        project.setName(nameNew);
        projectRepository.merge(project);
        return true;
    }

    public boolean remove(String projectName) {
        if (projectName == null || projectName.isEmpty()) return false;
        if (projectRepository.findOne(projectName) == null) return false;
        String projectId = projectRepository.findOne(projectName).getId();
        List<String> taskIdList = taskOwnerRepository.findAllByProjectId(projectId);
        if (taskIdList != null) {
            for (String taskId : taskIdList) {
                taskRepository.removeById(taskId); //вместе с удалением проекта удаляем все задачи проекта
            }
        }
        return projectRepository.remove(projectName);
    }

    public boolean remove(String projectName, String currentUserId) {
        if (projectName == null || projectName.isEmpty()) return false;
        if (currentUserId == null || currentUserId.isEmpty()) return false;
        if (projectRepository.findOne(projectName, currentUserId) == null) return false;
        String projectId = projectRepository.findOne(projectName, currentUserId).getId();
        List<String> taskIdList = taskOwnerRepository.findAllByProjectId(projectId);
        if (taskIdList != null) {
            for (String taskId : taskIdList) {
                if (taskId == null || taskId.isEmpty()) continue;
                taskRepository.removeById(taskId); //вместе с удалением проекта удаляем все задачи проекта
            }
        }
        return projectRepository.remove(projectName, currentUserId);
    }

    public void clear() {
        List<String> allTaskIdList = taskOwnerRepository.findAll();
        for (String taskId : allTaskIdList) {
            taskRepository.removeById(taskId); //удаляя все проекты, удаляем все задачи проектов
        }
        projectRepository.removeAll();
    }

    public void clear(String currentUserId) {
        if (currentUserId == null || currentUserId.isEmpty()) return;
        List<String> allTaskIdList = new ArrayList<>();
        List<Project> projects = projectRepository.findAll(currentUserId);
        for (Project project : projects) {
            if (taskOwnerRepository.findAllByProjectId(project.getId()) == null) continue;
            allTaskIdList.addAll(taskOwnerRepository.findAllByProjectId(project.getId()));
        }
        for (String taskId : allTaskIdList) {
            taskRepository.removeById(taskId); //удаляя все проекты, удаляем все задачи проектов
        }
        projectRepository.removeAll(currentUserId);
    }

    public boolean setTask(String projectName, String taskName) {
        if (projectName == null || projectName.isEmpty()) return false;
        if (taskName == null || taskName.isEmpty()) return false;
        if (projectRepository.findOne(projectName) == null) return false;
        if (taskRepository.findOne(taskName) == null) return false;
        String projectId = projectRepository.findOne(projectName).getId();
        String taskId = taskRepository.findOne(taskName).getId();
        taskOwnerRepository.merge(projectId, taskId);
        return true;
    }

    public boolean setTask(String projectName, String taskName, String currentUserId) {
        if (projectName == null || projectName.isEmpty()) return false;
        if (taskName == null || taskName.isEmpty()) return false;
        if (currentUserId == null || currentUserId.isEmpty()) return false;
        if (projectRepository.findOne(projectName, currentUserId) == null) return false;
        if (taskRepository.findOne(taskName, currentUserId) == null) return false;
        String projectId = projectRepository.findOne(projectName, currentUserId).getId();
        String taskId = taskRepository.findOne(taskName, currentUserId).getId();
        taskOwnerRepository.merge(projectId, taskId);
        return true;
    }

    public List<String> tasksList(String projectName) { //возвращает список задач проекта
        if (projectName == null || projectName.isEmpty()) return null;
        if (projectRepository.findOne(projectName) == null) return null;
        String projectId = projectRepository.findOne(projectName).getId();
        if (taskOwnerRepository.findAllByProjectId(projectId) == null) return null;
        List<String> taskIdList = taskOwnerRepository.findAllByProjectId(projectId);
        if (taskRepository.findAllById(taskIdList) == null) return null;
        List<Task> taskList = taskRepository.findAllById(taskIdList);
        List<String> taskNameList = new ArrayList<>();
        for (Task task : taskList) {
            taskNameList.add(task.getName());
        }
        return taskNameList;
    }

    public List<String> tasksList(String projectName, String currentUserId) { //возвращает список задач проекта
        if (projectName == null || projectName.isEmpty()) return null;
        if (currentUserId == null || currentUserId.isEmpty()) return null;
        if (projectRepository.findOne(projectName, currentUserId) == null) return null;
        String projectId = projectRepository.findOne(projectName, currentUserId).getId();
        if (taskOwnerRepository.findAllByProjectId(projectId) == null) return null;
        List<String> taskIdList = taskOwnerRepository.findAllByProjectId(projectId);
        if (taskRepository.findAllById(taskIdList) == null) return null;
        List<Task> taskList = taskRepository.findAllById(taskIdList);
        List<String> taskNameList = new ArrayList<>();
        for (Task task : taskList) {
            taskNameList.add(task.getName());
        }
        return taskNameList;
    }

}
