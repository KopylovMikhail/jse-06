package ru.kopylov.tm.service;

import ru.kopylov.tm.entity.User;
import ru.kopylov.tm.repository.UserRepository;
import ru.kopylov.tm.util.MD5Util;

public class UserService {

    private UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User merge(User user, String newPassword) {
        if (newPassword == null || newPassword.isEmpty()) return null;
        newPassword = MD5Util.hash(newPassword);
        user.setPassword(newPassword);
        userRepository.merge(user);
        return user;
    }

    public User updateLogin(User user, String newLogin) {
        if (newLogin == null || newLogin.isEmpty()) return null;
        user.setLogin(newLogin);
        userRepository.merge(user);
        return user;
    }

    public User persist(String login, String password) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        password = MD5Util.hash(password);
        if (userRepository.persist(login, password) == null) return null;
        return userRepository.persist(login, password);
    }

    public User findOne(String login, String password) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        password = MD5Util.hash(password);
        if (userRepository.findOne(login, password) == null) return null;
        return userRepository.findOne(login, password);
    }

    public User findById(String id) {
        if (id == null || id.isEmpty()) return null;
        if (userRepository.findById(id) == null) return null;
        return userRepository.findById(id);
    }
}
