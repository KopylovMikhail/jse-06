package ru.kopylov.tm.context;

import ru.kopylov.tm.command.*;
import ru.kopylov.tm.command.project.*;
import ru.kopylov.tm.command.system.ExitCommand;
import ru.kopylov.tm.command.system.HelpCommand;
import ru.kopylov.tm.command.task.*;
import ru.kopylov.tm.command.user.*;
import ru.kopylov.tm.entity.User;
import ru.kopylov.tm.enumerated.TypeRole;
import ru.kopylov.tm.repository.ProjectRepository;
import ru.kopylov.tm.repository.TaskOwnerRepository;
import ru.kopylov.tm.repository.TaskRepository;
import ru.kopylov.tm.repository.UserRepository;
import ru.kopylov.tm.service.ProjectService;
import ru.kopylov.tm.service.TaskService;
import ru.kopylov.tm.service.UserService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;

public class Bootstrap {

    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    private Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private ProjectRepository projectRepository = new ProjectRepository();

    private TaskRepository taskRepository = new TaskRepository();

    private TaskOwnerRepository taskOwnerRepository = new TaskOwnerRepository();

    private UserRepository userRepository = new UserRepository();

    private ProjectService projectService = new ProjectService(projectRepository, taskRepository, taskOwnerRepository);

    private TaskService taskService = new TaskService(taskRepository);

    private UserService userService = new UserService(userRepository);

    private User user = new User();

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public UserService getUserService() {
        return userService;
    }

    public BufferedReader getReader() {
        return reader;
    }

    public Map<String, AbstractCommand> getCommands() {
        return commands;
    }

    private void registry(AbstractCommand command) {
        commands.put(command.getName(), command);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void init() {
        {
            registry(new HelpCommand(this));
            registry(new ProjectCreateCommand(this));
            registry(new ProjectListCommand(this));
            registry(new ProjectUpdateCommand(this));
            registry(new ProjectRemoveCommand(this));
            registry(new ProjectClearCommand(this));
            registry(new ProjectSetTaskCommand(this));
            registry(new ProjectTasksListCommand(this));
            registry(new TaskCreateCommand(this));
            registry(new TaskListCommand(this));
            registry(new TaskUpdateCommand(this));
            registry(new TaskRemoveCommand(this));
            registry(new TaskClearCommand(this));
            registry(new UserLoginCommand(this));
            registry(new UserLogoutCommand(this));
            registry(new UserMyProfileCommand(this));
            registry(new UserRegistrationCommand(this));
            registry(new UserUpdateCommand(this));
            registry(new UserUpdateLoginCommand(this));
            registry(new ExitCommand(this));
        }
        userInit();
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String command = "";
        do {
            try {
                command = reader.readLine();
                execute(command);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } while (!"exit".equals(command));
    }

    private void execute(String command) {
        if (command == null || command.isEmpty()) return;
        if (checkPermission(command)) return;
        AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;
        abstractCommand.execute();
    }

    private boolean checkPermission(String command) {
        boolean contentProject = command.contains("project");
        boolean contentTask = command.contains("task");
        boolean contentUser = command.contains("user");
        boolean userNotAuth = (user.getRole() == null);
        return  ((contentProject || contentTask || contentUser) & userNotAuth);
    }

    private void userInit() {
        User admin = new User();
        admin.setLogin("admin");
        admin.setRole(TypeRole.ADMIN);
        User user = new User();
        user.setLogin("user");
        user.setRole(TypeRole.USER);
        userService.merge(admin, "111111");
        userService.merge(user, "222222");
    }

}
