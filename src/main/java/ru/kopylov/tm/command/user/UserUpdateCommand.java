package ru.kopylov.tm.command.user;

import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.context.Bootstrap;

import java.io.IOException;

public class UserUpdateCommand extends AbstractCommand {

    public UserUpdateCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "user-update";
    }

    @Override
    public String getDescription() {
        return "Update user password.";
    }

    @Override
    public void execute() {
        System.out.println("[USER PASSWORD UPDATE]\n" +
                "ENTER NEW PASSWORD:");
        try {
            String newPassword = bootstrap.getReader().readLine();
            if (bootstrap.getUserService().merge(bootstrap.getUser(), newPassword) == null) {
                System.out.println("PASSWORD IS EMPTY.\n");
                return;
            }
            bootstrap.setUser(bootstrap.getUserService().merge(bootstrap.getUser(), newPassword));
            System.out.println("[PASSWORD UPDATE SUCCESS]\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
