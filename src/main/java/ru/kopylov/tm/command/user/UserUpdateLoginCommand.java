package ru.kopylov.tm.command.user;

import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.context.Bootstrap;

import java.io.IOException;

public class UserUpdateLoginCommand extends AbstractCommand {

    public UserUpdateLoginCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "user-update-login";
    }

    @Override
    public String getDescription() {
        return "Update user login.";
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGIN UPDATE]\n" +
                "ENTER NEW LOGIN:");
        try {
            String newLogin = bootstrap.getReader().readLine();
            if (bootstrap.getUserService().updateLogin(bootstrap.getUser(), newLogin) == null) {
                System.out.println("LOGIN IS EMPTY.\n");
                return;
            }
            bootstrap.getUserService().updateLogin(bootstrap.getUser(), newLogin);
            System.out.println("[LOGIN UPDATE SUCCESS]\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
