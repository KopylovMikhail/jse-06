package ru.kopylov.tm.command.user;

import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.context.Bootstrap;

public class UserMyProfileCommand extends AbstractCommand {

    public UserMyProfileCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "my-profile";
    }

    @Override
    public String getDescription() {
        return "Show current user profile.";
    }

    @Override
    public void execute() {
        if (bootstrap.getUser() == null || bootstrap.getUser().getRole() == null) {
            System.out.println("YOU NEED LOGIN.\n");
            return;
        }
        System.out.println("[USER PROFILE]\n" +
                "LOGIN: " + bootstrap.getUser().getLogin() + "\n" +
                "ROLE: " + bootstrap.getUser().getRole().getDisplayName() + "\n");
//                + "HASH: " + bootstrap.getUser().getPassword() + "\n"); //проверка, что пароль захеширован
    }

}
