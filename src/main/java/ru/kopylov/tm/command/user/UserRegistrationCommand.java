package ru.kopylov.tm.command.user;

import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.context.Bootstrap;
import ru.kopylov.tm.util.MD5Util;

import java.io.IOException;

public class UserRegistrationCommand extends AbstractCommand {

    public UserRegistrationCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "registration";
    }

    @Override
    public String getDescription() {
        return "User registration.";
    }

    @Override
    public void execute() {
        System.out.println("[USER REGISTRATION]\n" +
                "ENTER LOGIN:");
        try {
            String login = bootstrap.getReader().readLine();
            System.out.println("[ENTER PASSWORD:");
            String password = bootstrap.getReader().readLine();
            if (bootstrap.getUserService().persist(login, password) == null) {
                System.out.println("SUCH USER ALREADY EXIST OR LOGIN/PASSWORD IS EMPTY.\n");
                return;
            }
            bootstrap.setUser(bootstrap.getUserService().persist(login, password));
            System.out.println("[REGISTRATION SUCCESS]\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
