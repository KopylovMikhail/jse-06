package ru.kopylov.tm.command.user;

import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.context.Bootstrap;
import ru.kopylov.tm.util.MD5Util;

import java.io.IOException;

public class UserLoginCommand extends AbstractCommand {

    public UserLoginCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "login";
    }

    @Override
    public String getDescription() {
        return "User authorization.";
    }

    @Override
    public void execute() {
        System.out.println("[USER AUTHORIZATION]\n" +
                "ENTER LOGIN:");
        try {
            String login = bootstrap.getReader().readLine();
            System.out.println("ENTER PASSWORD:");
            String password = bootstrap.getReader().readLine();
            if (bootstrap.getUserService().findOne(login, password) == null) {
                System.out.println("SUCH USER DOES NOT EXIST OR LOGIN/PASSWORD IS INCORRECT.\n");
                return;
            }
            bootstrap.setUser(bootstrap.getUserService().findOne(login, password));
            System.out.println("[AUTHORIZATION SUCCESS]\n" +
                    "HELLO, " + bootstrap.getUser().getLogin() + "!\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
