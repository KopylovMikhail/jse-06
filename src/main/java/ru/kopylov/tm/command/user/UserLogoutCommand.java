package ru.kopylov.tm.command.user;

import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.context.Bootstrap;
import ru.kopylov.tm.entity.User;

public class UserLogoutCommand extends AbstractCommand {

    public UserLogoutCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "logout";
    }

    @Override
    public String getDescription() {
        return "Complete user session.";
    }

    @Override
    public void execute() {
        bootstrap.setUser(new User());
        System.out.println("[LOGOUT SUCCESS]\n");
    }

}
