package ru.kopylov.tm.command.project;

import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.context.Bootstrap;

import java.io.IOException;
import java.util.List;

public class ProjectTasksListCommand extends AbstractCommand {

    public ProjectTasksListCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project-tasks-list";
    }

    @Override
    public String getDescription() {
        return "Project task list.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER EXISTING PROJECT NAME:");
        try {
            String projectName = bootstrap.getReader().readLine();
            if (bootstrap.getProjectService().tasksList(projectName) == null) {
                System.out.println("SUCH A PROJECT DOES NOT HAVE TASKS OR NAME IS EMPTY.\n");
                return;
            }
            List<String> taskList = bootstrap.getProjectService().tasksList(projectName, bootstrap.getUser().getId());
            System.out.println("TASKS LIST FOR PROJECT " + projectName + ":");
            int n = 1;
            for (String taskName : taskList) {
                System.out.println(n++ + ". " + taskName);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.print("\n");
    }

}
