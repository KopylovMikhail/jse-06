package ru.kopylov.tm.command.project;

import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.context.Bootstrap;

import java.io.IOException;

public class ProjectSetTaskCommand extends AbstractCommand {

    public ProjectSetTaskCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project-set-task";
    }

    @Override
    public String getDescription() {
        return "Assign a task to a project.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER EXISTING PROJECT NAME:");
        try {
            String projectName = bootstrap.getReader().readLine();
            System.out.println("ENTER EXISTING TASK NAME:");
            String taskName = bootstrap.getReader().readLine();
            if (bootstrap.getProjectService().setTask(projectName, taskName, bootstrap.getUser().getId()))
                System.out.println("[OK]\n");
            else System.out.println("SUCH A PROJECT/TASK DOES NOT EXIST OR NAME IS EMPTY.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
