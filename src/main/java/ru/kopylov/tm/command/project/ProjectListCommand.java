package ru.kopylov.tm.command.project;

import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.context.Bootstrap;
import ru.kopylov.tm.entity.Project;

import java.util.List;

public class ProjectListCommand extends AbstractCommand {

    public ProjectListCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project-list";
    }

    @Override
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    public void execute() {
        List<Project> projectList = bootstrap.getProjectService().list(bootstrap.getUser().getId());
        System.out.println("[PROJECT LIST]");
        int i = 1;
        for (Project project : projectList) {
            System.out.println(i++ + ". " + project.getName()  + ", created by: "
                    + bootstrap.getUserService().findById(project.getUserId()).getLogin());
        }
        System.out.print("\n");
    }

}
