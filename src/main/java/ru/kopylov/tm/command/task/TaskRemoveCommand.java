package ru.kopylov.tm.command.task;

import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.context.Bootstrap;

import java.io.IOException;

public class TaskRemoveCommand extends AbstractCommand {

    public TaskRemoveCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected task.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK REMOVE]\n" +
                "ENTER EXISTING TASK NAME:");
        try {
            String taskName = bootstrap.getReader().readLine();
            if (bootstrap.getTaskService().remove(taskName, bootstrap.getUser().getId()))
                System.out.println("[TASK " + taskName + " REMOVED]\n");
            else System.out.println("SUCH A TASK DOES NOT EXIST OR NAME IS EMPTY.\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
