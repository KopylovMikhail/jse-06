package ru.kopylov.tm.command.task;

import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.context.Bootstrap;

import java.io.IOException;

public class TaskCreateCommand extends AbstractCommand {

    public TaskCreateCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-create";
    }

    @Override
    public String getDescription() {
        return "Create new task.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK CREATE]\n" +
                "ENTER NAME:");
        try {
            String taskName = bootstrap.getReader().readLine();
            if (bootstrap.getTaskService().create(taskName, bootstrap.getUser().getId()))
                System.out.println("[OK]\n");
            else System.out.println("[SUCH A TASK EXISTS OR NAME IS EMPTY.]\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
