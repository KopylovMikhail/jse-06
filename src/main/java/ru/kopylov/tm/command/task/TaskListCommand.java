package ru.kopylov.tm.command.task;

import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.context.Bootstrap;
import ru.kopylov.tm.entity.Task;

import java.util.List;

public class TaskListCommand extends AbstractCommand {

    public TaskListCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    public String getDescription() {
        return "Show all tasks.";
    }

    @Override
    public void execute() {
        List<Task> taskList = bootstrap.getTaskService().list(bootstrap.getUser().getId());
        System.out.println("[TASK LIST]");
        int i = 1;
        for (Task task : taskList) {
            System.out.println(i++ + ". " + task.getName()  + ", created by: "
                    + bootstrap.getUserService().findById(task.getUserId()).getLogin());
        }
        System.out.print("\n");
    }

}
