package ru.kopylov.tm.command.task;

import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.context.Bootstrap;

import java.io.IOException;

public class TaskUpdateCommand extends AbstractCommand {

    public TaskUpdateCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-update";
    }

    @Override
    public String getDescription() {
        return "Update selected task.";
    }

    @Override
    public void execute() {
        String nameOld, nameNew;
        System.out.println("[TASK UPDATE]\n" +
                "ENTER EXISTING TASK NAME:");
        try {
            nameOld = bootstrap.getReader().readLine();
            System.out.println("ENTER NEW TASK NAME:");
            nameNew = bootstrap.getReader().readLine();
            if (bootstrap.getTaskService().update(nameOld, nameNew, bootstrap.getUser().getId()))
                System.out.println("[TASK " + nameOld + " UPDATED TO " + nameNew + "]\n");
            else System.out.println("SUCH A TASK DOES NOT EXIST OR NAME IS EMPTY.\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
